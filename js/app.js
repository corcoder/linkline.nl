// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();

// Update the footer position on page load
$(window).bind("load", function () {
	updateFooterPosition();
});

// Update the footer position on window resize
$(window).resize(function() {
	updateFooterPosition();
});

// Update the footer position to make it sticky
function updateFooterPosition() {
	var footer = $("footer");
	var pos = footer.position();
	var height = $(window).height();
	height = height - pos.top;
	height = height - footer.height();
	if (height > 0) {
		footer.css({
			'margin-top': height + 'px'
		});
	}

}
