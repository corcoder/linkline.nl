'use strict';

// requirements
var gulp = require('gulp');
var sass = require('gulp-sass');
var compass = require('gulp-compass');
var process = require('child_process');
var shell = require('gulp-shell');
var autoprefixer = require('gulp-autoprefixer');


var paths = {
	styles: {
		src: 'scss/*.scss',
		dest: 'stylesheets'
	}
};

gulp.task('default', ['scss_watch', 'start_server']);

gulp.task('scss_watch', function() {
	gulp.watch('scss/**/*.scss', ['compass']);
});

gulp.task('compass', function() {
	return gulp.src(paths.styles.src)
	.pipe(compass({
		config_file: './config.rb',
		css: 'stylesheets',
		sass: 'scss'
	})).pipe(autoprefixer({
		browsers: ['last 2 versions'],
		cascade: false
	}))
	.pipe(gulp.dest(paths.styles.dest));
});

gulp.task('start_server', shell.task(['python3 -m http.server 5000']));